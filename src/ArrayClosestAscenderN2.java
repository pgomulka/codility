
public class ArrayClosestAscenderN2 {
	// you can also use imports, for example:
	// import java.math.*;
	class Solution {
	    public int[] solution(int[] A) {
	        int [] R = new int[A.length];
	        for(int i=0;i<A.length;i++){
	        	
	            int c = findClosestAscender(i,A);
	            R[i]=c;
	        }
	        return R;
	    }
	    private int findClosestAscender(int k, int[] A){
	    	int dist = Integer.MAX_VALUE;
	        boolean found = false;
	        for(int i=0;i<A.length;i++){
	            if(A[i]>A[k]){
	            	dist = Math.min(dist,Math.abs(i-k));
	                found = true;
	            }
	        }
	        
	        return found?dist:0;
	    }
	}

}
