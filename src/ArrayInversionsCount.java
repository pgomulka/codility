import java.util.Arrays;

public class ArrayInversionsCount {
	 public int solution(int[] A) {
		 int[] helper = new int[A.length];
			int c = sort(0, A.length - 1,A,helper);
			return c;
	
	 }

		private int sort(int begining, int end,int[] array,int[] helper) {
			int changes = 0;
			if (end - begining>0 ) {
				int middle = (end - begining) / 2 + begining;
				changes+=sort(begining, middle,array,helper);
				changes+=sort(middle+1, end,array,helper);
				changes+=merge(begining, end,array,helper);
			}
			return changes;
		}

		public int merge(int begining, int end,int[] array,int[] helper) {
			int changes=0;
			for(int i=begining;i<=end;i++)
				helper[i]=array[i];
			int middle = (end - begining) / 2 + begining;
			int i = begining;
			int j = middle+1;
			int p = begining;
			while (i <= middle && j <= end) {
				if (helper[i] <= helper[j]) {
					array[p++] = helper[i++];
				} else {
					array[p++] = helper[j++];
					changes+=middle+1-i;
					// System.out.println("f1 "+array[j-1]);
				}
			}
			for (; i <= middle; i++, p++) {
				array[p] = helper[i];
			}
			for (; j <= end; j++, p++)
				array[p] = helper[j];
			return changes;
		}
		public static void main(String[] args) {
			ArrayInversionsCount a = new ArrayInversionsCount();
			int solution = a.solution(new int[]{-1, 6, 3, 4, 7,4});
			System.out.println(solution );
		}
}
