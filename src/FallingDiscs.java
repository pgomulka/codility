import java.util.Arrays;

public class FallingDiscs {
	public int solution(int[] A, int[] B) {
		int lastDisk = -1;
		int fallen = 0;

		for (int i = 0; i < B.length; i++) {

			int j = findPositionForDisc(A, B[i], lastDisk);
			if (j >= 0) {
				lastDisk = j;
				fallen++;
			} else
				return fallen;

		}

		return fallen;
	}

	private int findPositionForDisc(int[] A, int disc, int lastDisk) {
		int j = 0;
		while (j < A.length && (lastDisk == -1 || lastDisk > j) && fits(disc, A[j]))
			j++;
		return j-1;
	}

	private boolean fits(int disc, int ring) {
		return disc <= ring;
	}

	

	public static void main(String[] args) {
		FallingDiscs d = new FallingDiscs();
		int solution = d.solution(new int[] { 5, 6, 4, 3, 6, 2, 3 }, new int[] { 2, 3, 5, 2, 4 });
		System.out.println(solution);
		solution = d.solution(new int[] { 5, 5, 5 }, new int[] { 4, 4, 4 });
		System.out.println(solution);
		solution = d.solution(new int[] { 5 }, new int[] { 4, 4, 4 });
		System.out.println(solution);
		solution = d.solution(new int[] { 1, 1, 1 }, new int[] { 4, 4, 4 });
		System.out.println(solution);
		solution = d.solution(new int[] { 1, 1, 3 }, new int[] { 2 });
		System.out.println(solution);
	}
}
