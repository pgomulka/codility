
public class FallingDiscsN {
	public int solution(int[] A, int[] B) {
		transform(A);

		int k = A.length - 1;
		int fallen = 0;
		for (int i = 0; i < B.length; i++) {
			for (; k >= 0; k--) {
				if (B[i] <= A[k]) {
					fallen++;
					k--;
					break;
				}
			}
		}

		return fallen;
	}

	private int[] transform(int[] A) {
		int m = Integer.MAX_VALUE;
		for (int i = 0; i < A.length; i++) {
			m = Math.min(A[i], m);
			A[i] = m;
		}
		return A;
	}

	public static void main(String[] args) {
		FallingDiscsN d = new FallingDiscsN();
		;
		int solution = d.solution(new int[] { 5, 6, 4, 3, 6, 2, 3 }, new int[] { 2, 3, 5, 2, 4 });
		System.out.println(solution);
		solution = d.solution(new int[] { 5, 5, 5 }, new int[] { 4, 4, 4 });
		System.out.println(solution);
		solution = d.solution(new int[] { 5 }, new int[] { 4, 4, 4 });
		System.out.println(solution);
		solution = d.solution(new int[] { 1, 1, 1 }, new int[] { 4, 4, 4 });
		System.out.println(solution);
		solution = d.solution(new int[] { 1, 1, 3 }, new int[] { 2 });
		System.out.println(solution);
	}
}
