public class FrogJump {

	public int solution(int X, int Y, int D) {
		if (Y > X)
			return (int) Math.ceil((Y - X) / (double) D);
		else
			return (int) Math.ceil((X-Y) / (double) D);
	}

	public static void main(String[] args) {
		FrogJump s = new FrogJump();
		int solution = s.solution(10, 85, 30);
		System.out.println(solution);
	}
}
