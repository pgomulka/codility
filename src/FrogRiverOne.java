public class FrogRiverOne {
	public int solution(int X, int[] A) {
		int[] leaves = createArray(X, -1);

		simulateLeafFall(A, leaves);

		return findLastLeaf(X, leaves);
	}

	private void simulateLeafFall(int[] A, int[] leaves) {
		for (int k = 0; k < A.length; k++) {
			int leafPosition = A[k]-1;
			if (leaves[leafPosition] == -1)
				leaves[leafPosition] = k;
		}
	}

	private int findLastLeaf(int X, int[] leaves) {
		int max = 0;
		for (int i = 0; i < X; i++) {
			if (leaves[i] == -1)
				return -1;
			max = Math.max(leaves[i], max);
		}
		return max;
	}

	private int[] createArray(int x, int value) {
		int[] array = new int[x];
		for (int i = 0; i < x; i++)
			array[i] = value;
		return array;
	}

	public static void main(String[] args) {
		FrogRiverOne f = new FrogRiverOne();
		int solution = f.solution(5,new int[]{1,3,1,4,2,3,5,4});
		System.out.println(solution);
		solution = f.solution(5,new int[]{1,3,1,4,2,3,1,4});
		System.out.println(solution);
		solution = f.solution(1,new int[]{1});
		System.out.println(solution);
	}
}
