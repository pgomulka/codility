import java.util.Arrays;

public class MaxCounters {
	  public int[] solution(int N, int[] A)
	    {
	        int[] counter = new int[N];
	        int max = 0;
	        int lastMax = 0;
	        int s = 1;
	        int[] used = new int[N];
	        for (int i = 0; i < A.length; i++)
	        {
	            if (A[i] == N + 1)
	            {
	                s++;
	                lastMax = max;
	            }
	            else
	            {
	                int t;
	                int x = A[i] - 1;
	                if (used[x] != s)
	                    t = counter[x] = 1;
	                else
	                    t = ++counter[x];
	                max = Math.max(max, t);
	                used[x] = s;
	            }

	        }
	        
	        for(int i=0;i<N;i++){
	            if(used[i]!=s)
	                counter[i] = lastMax;
	            else
	                counter[i]+=lastMax;
	        }
	        return counter;
	    }

//	public int[] solution2(int N, int[] A) {
//		int maxValue = getLastMaxValue2(N, A);
//		return calculateCounters(N, maxValue, A);
//	}
//
//	private int[] calculateCounters(int N, int maxValue, int[] A) {
//		int maxIndex = findLastMaxIndex(N, A);
//		int[] counters = createArray(N, maxValue);
//		
//		for (int i = maxIndex; i < A.length; i++) {
//			if (A[i] != N + 1)
//		return counters;
//	}
//
//	private int[] createArray(int N, int value) {
//		int[] counters = new int[N];
//		for (int i = 0; i < N; i++) {
//			counters[i] = value;
//		}
//		return counters;
//	}
//
//	private int getLastMaxValue2(int N, int[] A) {
//		int max = 0,lastMax = 0;
//		int[] occ  = new int[N];
//		
//		for (int i = 0; i < A.length; i++) {
//			if (A[i] == N + 1) {
//				occ = new int[N];
//				lastMax = max;
//			} else {
//				int t = ++occ[A[i]-1];
//				max = Math.max(max, t);
//			}
//		}
//		return lastMax;
//	}
//	private int findLastMaxIndex(int N, int[] operations) {
//		int last = 0;
//		for (int i = 0; i < operations.length; i++) {
//			if (operations[i] == N + 1)
//				last = i;
//		}
//		return last;
//	}

	public static void main(String[] args) {
		MaxCounters s = new MaxCounters();
		int[] solution = s.solution(5, new int[] { 3, 4, 4, 6, 1, 4, 4 });
		System.out.println(Arrays.toString(solution));
		solution = s.solution(5, new int[] { 3, 4, 4 });
		System.out.println(Arrays.toString(solution));
		solution = s.solution(5, new int[] { 6, 6, 6 });
		System.out.println(Arrays.toString(solution));
		solution = s.solution(5, new int[] { 3, 4, 6 });
		System.out.println(Arrays.toString(solution));
		solution = s.solution(5, new int[] { 6, 4, 4,6 ,4,4});
		System.out.println(Arrays.toString(solution));
	}
}
