import java.util.Arrays;

public class MergeSort {
	int[] helper;
	private final int[] array;
	int changes = 0;

	MergeSort(int[] array) {
		this.array = array;
		helper = new int[array.length];
	}

	public int[] sort() {
		sort(0, array.length - 1);
		return array;
	}

	private int[] sort(int begining, int end) {
		if (end - begining>0 ) {
			int middle = (end - begining) / 2 + begining;
			sort(begining, middle);
			sort(middle+1, end);
			merge(begining, end);
		}
		return helper;
	}

	public int[] merge(int begining, int end) {
		for(int i=begining;i<=end;i++)
			helper[i]=array[i];
		int middle = (end - begining) / 2 + begining;
		int i = begining;
		int j = middle+1;
		int p = begining;
		while (i <= middle && j <= end) {
			if (helper[i] < helper[j]) {
				array[p++] = helper[i++];
			} else {
				array[p++] = helper[j++];
				changes+=middle+1-i;
				// System.out.println("f1 "+array[j-1]);
			}
		}
		for (; i <= middle; i++, p++) {
			array[p] = helper[i];
		}
		for (; j <= end; j++, p++)
			array[p] = helper[j];
		return helper;
	}

	public static void main(String[] args) {

		printSorted(1);
		printSorted(2, 1);
		printSorted(8, 1, 6);
		printSorted(1, 3, 2);
		printSorted(1, 6, 8, 2, 3, 4);

	}

	private static void printSorted(int... nums) {
		MergeSort m = new MergeSort(nums);
		System.out.println(Arrays.toString(m.sort()) + " " + m.changes);
		// TODO Auto-generated method stub

	}
}
