import java.util.*;

public class PermCheck {
	public int solution(int[] A) {
		Map<Integer, Integer> m = new HashMap<Integer, Integer>();
		for (int i = 0; i < A.length; i++) {
			if (m.containsKey(A[i])) {
				int v = m.get(A[i]);
				m.put(A[i], v + 1);
			} else
				m.put(A[i], 1);
		}

		if (m.size() == A.length - 1) {
			for (Integer key : m.keySet()) {
				if (m.get(key) > 1)
					return 0;
			}
			return 1;

		}
		return 0;
	}

	public int solution2(int[] A) {
		int[] occurences = new int[A.length];
		for (int i = 0; i < A.length; i++) {
			if (A[i] <= A.length) {
				occurences[A[i]-1]++;
			} else
				return 0;
		}
		
		for(int i =0;i<A.length;i++)
			if(occurences[i]!=1)
				return 0;
		return 1;
	}

	public static void main(String[] args) {
		PermCheck p = new PermCheck();
		System.out.println(p.solution2(new int[] { 1, 2, 3 }));

		System.out.println(p.solution2(new int[] { 1}));

		System.out.println(p.solution2(new int[] { 1,3}));
		System.out.println(p.solution2(new int[] { 1,3,3}));

	}
}
