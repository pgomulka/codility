import java.util.Arrays;

public class StoneWallN {
	public int solution(int[] H) {
		int[] blocksMap = new int[H.length];
		int blocks = 0;
		int[] stack = new int[H.length];
		int stackCounter = 0;
		for (int i = 0; i < H.length; i++) {
			while(stackCounter>0 && stack[stackCounter-1]>H[i])
				stackCounter--;
			
			if (stackCounter==0 || stack[stackCounter-1] < H[i]) {
				stack[stackCounter] = H[i];
				stackCounter++;
				blocks++;
			}

		}

		System.out.println(Arrays.toString(stack));
		return blocks;
	}

	public static void main(String[] args) {
		StoneWallN s = new StoneWallN();
		int solution = s.solution(new int[] { 8, 8, 5, 7, 9, 8, 7, 4, 8 });
		System.out.println(solution);
		solution = s.solution(new int[] { 8 });
		System.out.println(solution);
		solution = s.solution(new int[] { 8, 8 });
		System.out.println(solution);
		solution = s.solution(new int[] { 8, 8, 8 });
		System.out.println(solution);
		solution = s.solution(new int[] { 8, 5, 8 });
		System.out.println(solution);
	}
}
