import java.util.Arrays;

public class StoneWallNSquare {

	public int solution(int[] H) {
		int[] blocksMap = new int[H.length];

		int[] coverage = new int[H.length];
		int blocks = 0;
		for (int pos = 0; pos < H.length; pos++) {
			int height = H[pos];
			if (!wasCovered(height, pos, coverage)) {
				maximizeBlock(coverage, H, pos,blocks,blocksMap);
				blocks++;
			}
		}
		System.out.println(Arrays.toString(blocksMap));
		return blocks;
	}

	private void maximizeBlock(int[] coverage, int[] H, int pos, int blocks, int[] blocksMap) {
		int i = pos;
		int height = H[pos];
		while (i<H.length && H[i] >= height ) {
			coverage[i] = height;
			blocksMap[i]=blocks;
			i++;
		}
	}

	private boolean wasCovered(int height, int pos, int[] coverage) {
		int currentHeight = coverage[pos];
		return currentHeight >= height;
	}

	public static void main(String[] args) {
		StoneWallNSquare s = new StoneWallNSquare();
		int solution = s.solution(new int[] { 8, 8, 5, 7, 9, 8, 7, 4, 8 });
		System.out.println(solution);
		solution = s.solution(new int[] { 8, 8});
		System.out.println(solution);
		solution = s.solution(new int[] { 8,8,8});
		System.out.println(solution);
		solution = s.solution(new int[] { 8, 5,8});
		System.out.println(solution);
	}
}
