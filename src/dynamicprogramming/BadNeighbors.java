/**
 * Created by przemek on 21.02.15.
 */
public class BadNeighbors {
    public static void main(String[] args) {
        int[] x = {10, 3, 2, 5, 7, 8};
        maxDonations(x);

        maxDonations(new int[]{11, 15});
//        Returns: 15
//        2)

        maxDonations(new int[]{7, 7, 7, 7, 7, 7, 7});
//        Returns: 21
//        3)

        maxDonations(new int[]{1, 2, 3, 4, 5, 1, 2, 3, 4, 5});
//        Returns: 16
//        4)

        maxDonations(new int[]{94, 40, 49, 65, 21, 21, 106, 80, 92, 81, 679, 4, 61,
                6, 237, 12, 72, 74, 29, 95, 265, 35, 47, 1, 61, 397,
                52, 72, 37, 51, 1, 81, 45, 435, 7, 36, 57, 86, 81, 72});
//        Returns: 2926
//247.36 points
    }

    private static int maxDonations(int[] A) {
        int[] S = new int[A.length];
        for (int i = 0; i < A.length; i++) {
            S[i] = A[i];
        }
        int[] S2 = new int[A.length];
        for (int i = 0; i < A.length; i++) {
            S2[i] = A[i];
        }
        for (int i = 0; i < A.length - 1; i++) {
            for (int j = 0; j < i - 1; j++) {
                if (A[i] + S[j] > S[i]) {
                    S[i] = A[i] + S[j];
                }
            }
        }
        for (int i = 1; i < A.length ; i++) {
            for (int j = 1; j < i-1; j++) {
                if (A[i] + S2[j] > S2[i]) {
                    S2[i] = A[i] + S2[j];
                }
            }
        }
        S[A.length-1]=S2[A.length-1];
//        System.out.println(Arrays.toString(cf));
//        System.out.println(Arrays.toString(S));
//        System.out.println(Arrays.toString(S2));
//        return A
        int max = S[0];
        for(int i=0;i<S.length;i++){
            if(S[i]>max){
                max = S[i];
            }
        }
//        System.out.println(Arrays.toString(A)+" "+max);
        return max;
    }
}
