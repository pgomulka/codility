import scala.collection.JavaConversions._
import scala.math._
// you can use println for debugging purposes, e.g.
// println("this is a debug message")

object Solution {
    def solution(A: Array[Int]): Int = {
        var S = new Array[Int](A.length)
        S(0)=A(0);
        for ( i <- 1 to A.length-1){
           var m = bestPrevField(i,S);
           S(i) = m+A(i);
         
        }
        return S(A.length-1);
    }
    
    def bestPrevField(i : Int,S : Array[Int]): Int = { 
        val last6Field = S.slice(max(0,i-6),i);
        return last6Field.max;
    }
}

