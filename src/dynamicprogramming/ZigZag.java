import java.util.Arrays;

/**
 * Created by przemek on 17.02.15.
 */
public class ZigZag {
    public static void main(String[] args) {
        int[] x = {1, 16, 30, 29, 30};
        zigzag( new int[]{ 1, 7, 4, 9, 2, 5 });
        zigzag( new int[]{ 1, 17, 5, 10, 13, 15, 10, 5, 16, 8 });
        zigzag( new int[] { 44 });
        zigzag( new int[]  { 1, 2, 3, 4, 5, 6, 7, 8, 9 });
        zigzag( new int[]  { 70, 55, 13, 2, 99, 2, 80, 80, 80, 80, 100, 19, 7, 5, 5, 5, 1000, 32, 32 });
        zigzag( new int[]   { 374, 40, 854, 203, 203, 156, 362, 279, 812, 955,
                600, 947, 978, 46, 100, 953, 670, 862, 568, 188,
                67, 669, 810, 704, 52, 861, 49, 640, 370, 908,
                477, 245, 413, 109, 659, 401, 483, 308, 609, 120,
                249, 22, 176, 279, 23, 22, 617, 462, 459, 244 });

       /*
        { 1, 7, 4, 9, 2, 5 }
        Returns: 6
        The entire sequence is a zig-zag sequence.
        1)

        { 1, 17, 5, 10, 13, 15, 10, 5, 16, 8 }
        Returns: 7
        There are several subsequences that achieve this length. One is 1,17,10,13,10,16,8.
        2)

        { 44 }
        Returns: 1
        3)

        { 1, 2, 3, 4, 5, 6, 7, 8, 9 }
        Returns: 2
        4)

        { 70, 55, 13, 2, 99, 2, 80, 80, 80, 80, 100, 19, 7, 5, 5, 5, 1000, 32, 32 }
        Returns: 8
        5)

        { 374, 40, 854, 203, 203, 156, 362, 279, 812, 955,
                600, 947, 978, 46, 100, 953, 670, 862, 568, 188,
                67, 669, 810, 704, 52, 861, 49, 640, 370, 908,
                477, 245, 413, 109, 659, 401, 483, 308, 609, 120,
                249, 22, 176, 279, 23, 22, 617, 462, 459, 244 }
        Returns: 36*/
    }

    private static int zigzag(int[] A) {

        if (A.length == 1)
            return 1;
        int[] dp = new int[A.length];

        for (int i = 0; i < A.length; i++) {
            dp[i] = 1;
        }

        boolean[] t = new boolean[A.length];

        for (int i = 1; i < A.length; i++) {

            for (int j = 0; j < i; j++) {
                if ((A[i] - A[j] !=0 && A[i] - A[j] > 0 != t[j] )|| j == 0) {
                    if (dp[j] + 1 > dp[i]) {

                        dp[i] = dp[j] + 1;
                        t[i] = A[i] - A[j] > 0;
                    }
                }

            }
        }
//        for(int i=0;i<A.length;i++){
//            System.out.println(A[i]+"\t"+dp[i]+" "+t[i]);
////        }
//        System.out.println(Arrays.toString(A));
//        System.out.println(Arrays.toString(dp));
//        System.out.println(Arrays.toString(t));
//        System.out.println();
        return dp[A.length - 1];
    }
}
